﻿var Hapi = require("hapi");
var config = require("./src/config");
var staticRoutes = require("./src/routes/static");

var server = new Hapi.Server();

server.connection({
    port: config.port
});

server.route(staticRoutes);

server.start(function () {
    console.log("Server listening on port %d", config.port);
});
//# sourceMappingURL=server.js.map
