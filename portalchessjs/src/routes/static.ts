﻿export = routes;

var routes: Hapi.RouteOptions[] = [];

routes.push({
    method: 'GET',
    path: '/{param*}',
    handler: {
        directory: {
            path: 'src/static'
        }
    }
});