﻿
var routes = [];

routes.push({
    method: 'GET',
    path: '/{param*}',
    handler: {
        directory: {
            path: 'src/static'
        }
    }
});
module.exports = routes;
//# sourceMappingURL=static.js.map
