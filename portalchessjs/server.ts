﻿import Hapi = require("hapi");
import config = require("./src/config");
import staticRoutes = require("./src/routes/static");

var server = new Hapi.Server();

server.connection({
    port: config.port
});

server.route(staticRoutes);

server.start(() => {
    console.log("Server listening on port %d", config.port);
});